package cm.cadventure;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.HashMap;


import bbdd.Genera;
import bbdd.InventarioMapper;
import bbdd.ItemMapper;
import bbdd.MisionMapper;
import bbdd.PersonajeMapper;
import bbdd.PreguntaMapper;


public class ChooseGameActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_game);
        View nueva = findViewById(R.id.NuevaPartidabtn);
        nueva.setOnClickListener(this);
        View continuar = findViewById(R.id.Continuar);
        continuar.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent i;
        Personaje character;
        Partida game;
        Genera gen;
        MisionMapper mm;
        ItemMapper itemM;
        PersonajeMapper pm;
        PreguntaMapper askm;
        InventarioMapper invM;

        switch(v.getId()){
            case R.id.NuevaPartidabtn:
                i = new Intent(this,PlayGame.class);
                character = new Personaje(1,"Usuario","drawable/heroe_default.jpg",100,0,0,0,new Inventario(1));
                gen = new Genera();
                itemM = new ItemMapper(this);
                pm = new PersonajeMapper(this);
                mm = new MisionMapper(this);
                askm = new PreguntaMapper(this);
                mm.insert(gen.getM1());
                askm.insert(gen.getP1());
                askm.insert(gen.getP2());
                askm.insert(gen.getP3());
                itemM.insert(gen.getI1());
                pm.insert(character);
               // character = pm.findById(1);
                game = new Partida(character);
               // game.setPosada(mm.findMisions());
                //game.setItemColection(itemM.findItems());
                i.putExtra("PARCELABLE",game);
                startActivity(i);
                break;
            case R.id.Continuar:
                i = new Intent(this,PlayGame.class);
                //invM = new InventarioMapper(this);
                pm = new PersonajeMapper(this);
                //mm = new MisionMapper(this);
                character = pm.findById(1);
                game = new Partida(character);
                //game.setPosada(mm.findMisions());

                //game.setItemColection(invM.findItems());
                i.putExtra("PARCELABLE",game);
                startActivity(i);
                break;
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_choose_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
