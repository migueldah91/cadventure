package cm.cadventure;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.Drawable;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;

//import bbdd.AbstractMapper;
import bbdd.Genera;
import bbdd.InventarioMapper;
import bbdd.MisionMapper;
import bbdd.PersonajeMapper;
import bbdd.PreguntaMapper;
import bbdd.ProyectoSQLiteHelper;


public class PlayActivity extends Activity implements View.OnClickListener {


    private ArrayList<Pregunta> preguntas = new ArrayList<Pregunta>();
    private ArrayList<Integer> hecha = new ArrayList<Integer>();
    private Pregunta actual;
    TextView descripcion;
    Button op1;
    Button op2;
    Button op3;
    Mision mis = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        TextView titulo = (TextView)findViewById(R.id.nombreMision);
        ImageView imagen = (ImageView)findViewById(R.id.imageView);
        this.descripcion = (TextView)findViewById(R.id.pregunta);
        this.op1 = (Button)findViewById(R.id.opcion1);
        this.op1.setOnClickListener(this);
        this.op2 = (Button)findViewById(R.id.opcion2);
        this.op2.setOnClickListener(this);
        this.op3 = (Button)findViewById(R.id.opcion3);
        this.op3.setOnClickListener(this);





        String nombreMision = getIntent().getStringExtra("MISION");

        //Si hemos abierto correctamente la base de datos

            MisionMapper misionmap = new MisionMapper(this);

            mis = misionmap.findById(nombreMision);


            titulo.setText(mis.getNombre());

            String nombre=mis.getImagen(); //nombre fichero sin extensión

            String recurso="drawable";

            int res_imagen = getResources().getIdentifier(nombre, recurso, getPackageName());
            imagen.setBackgroundResource(res_imagen);

            PreguntaMapper preguntamp = new PreguntaMapper(this);

           /* ProyectoSQLiteHelper bbdd = new ProyectoSQLiteHelper(this,"PROYECTO.db", null, 1);
            SQLiteDatabase db = bbdd.getWritableDatabase();

            Cursor cursor;
            if(db != null)
            {
                String[] strings = {mis.getNombre()};
                cursor = db.rawQuery(" SELECT ID FROM PREGUNTA,MISION WHERE MISION = ? ", strings);
                cursor.moveToFirst();
                int m = 0;
                while (!cursor.isNull(0)){

                    preguntas.add(preguntamp.findById(cursor.getInt(0)));
                    hecha.add(m,0);
                    m++;

                }

                //Cerramos la base de datos
                db.close();
            }*/

        this.preguntas = preguntamp.buscaPreguntas(nombreMision);
        this.actual = this.preguntas.get(0);

        this.descripcion.setText(preguntas.get(0).getDescripcion());
        this.op1.setText(preguntas.get(0).getOpc1());
        this.op2.setText(preguntas.get(0).getOpc2());
        this.op3.setText(preguntas.get(0).getOpc3());






    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */


    @Override
    public void onClick(View v) {

    /**

        int i;
        for(i = 0; i< preguntas.size();i++){

            if (i == 2){
                Intent k = new Intent(this,PlayGame.class);
                startActivity(k);
            }
            else{
                if ((i == 1) &&(hecha.get(0)== 1)){
                    hecha.add(i,1);
                    break;
                }
                else if ((i == 0)) {

                    hecha.add(i,1);
                    break;
>>>>>>> a19721fc28c90096ce38442c1c441eaa099e0646
                }
                i++;

<<<<<<< HEAD
                    i++;
                    this.onCreateDescription();
                   /* descripcion.setText(preguntas.get(i).getDescripcion());
                    op1.setText(preguntas.get(i).getId1());
                    op2.setText(preguntas.get(i).getId2());
                    op3.setText(preguntas.get(i).getId3());*/

//            }
   //  **/

            switch(v.getId()){
                case R.id.opcion1:
                    if (this.actual.getId1() == 0){
                        Intent k = new Intent(this,PlayGame.class);
                        k.putExtra("MISION", true);
                        if (!this.mis.getRecompensa().equalsIgnoreCase("No hay recompensa")){
                            k.putExtra("ITEM", this.mis.getRecompensa());
                        }
                        startActivity(k);
                        break;
                    }
                    cambiarVista(this.actual.getId1());
                    break;
                case R.id.opcion2:
                    if (this.actual.getId2() == 0){
                        Intent k = new Intent(this,PlayGame.class);
                        k.putExtra("MISION",true);
                        if (!this.mis.getRecompensa().equalsIgnoreCase("No hay recompensa")){
                            k.putExtra("ITEM", this.mis.getRecompensa());
                        }
                        startActivity(k);
                        break;
                    }
                    cambiarVista(this.actual.getId2());
                    break;
                case R.id.opcion3:
                    if (this.actual.getId3() == 0){
                        Intent k = new Intent(this,PlayGame.class);
                        k.putExtra("MISION",true);
                        if (!this.mis.getRecompensa().equalsIgnoreCase("No hay recompensa")){
                            k.putExtra("ITEM", this.mis.getRecompensa());
                        }
                        startActivity(k);
                        break;
                    }
                    cambiarVista(this.actual.getId3());
                    break;
            }

        }




    private void cambiarVista(int i) {

        Pregunta preg = null;
        for(Pregunta p: this.preguntas) {
            if (p.getId() == i){
                preg = p;
                break;
            }
        }
        this.actual = preg;
        this.descripcion.setText(preg.getDescripcion());
        this.op1.setText(preg.getOpc1());
        this.op2.setText(preg.getOpc2());
        this.op3.setText(preg.getOpc3());


    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
