package cm.cadventure;

/**
 * Clase con los atributos de una pregunta
 * Created by Miguel on 28/04/2015.
 */
public class Pregunta
{
    private int id;
    private String descripcion;
    private String opc1;
    private String opc2;
    private String opc3;
    private String mision;
    private int id1;
    private int id2;
    private int id3;


    public int getId1() {
        return id1;
    }

    public void setId1(int id1) {
        this.id1 = id1;
    }

    public int getId2() {
        return id2;
    }

    public void setId2(int id2) {
        this.id2 = id2;
    }

    public int getId3() {
        return id3;
    }

    public void setId3(int id3) {
        this.id3 = id3;
    }

    public Pregunta(int id, String descripcion, String opc1, String opc2, String opc3, String mision, int id1, int id2, int id3) {

        this.id = id;
        this.descripcion = descripcion;
        this.opc1 = opc1;
        this.opc2 = opc2;
        this.opc3 = opc3;
        this.mision = mision;
        this.id1 = id1;
        this.id2 = id2;
        this.id3 = id3;

    }


    public int getId() {

        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getOpc1() {
        return opc1;
    }

    public void setOpc1(String opc1) {
        this.opc1 = opc1;
    }

    public String getOpc2() {
        return opc2;
    }

    public void setOpc2(String opc2) {
        this.opc2 = opc2;
    }

    public String getOpc3() {
        return opc3;
    }

    public void setOpc3(String opc3) {
        this.opc3 = opc3;
    }

    public String getMision() {
        return mision;
    }

    public void setMision(String mision) {
        this.mision = mision;
    }
}
