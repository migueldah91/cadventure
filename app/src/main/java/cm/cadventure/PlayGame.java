package cm.cadventure;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import bbdd.InventarioMapper;
import bbdd.ItemMapper;
import bbdd.MisionMapper;
import bbdd.PersonajeMapper;
import bbdd.StatMapper;


public class PlayGame extends Activity implements View.OnClickListener {

    Partida game;
    Personaje personaje;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_game);
        TextView charname = (TextView) findViewById(R.id.Charname);
        this.img = (ImageView) findViewById(R.id.Avatar);



        Bundle extra = getIntent().getExtras();
        if (extra.getBoolean("MISION")){
            PersonajeMapper pers = new PersonajeMapper(this);
            this.personaje = pers.findById(1);
            this.game = new Partida();
            this.game.setPersonaje(this.personaje);
            MisionMapper misiones = new MisionMapper(this);
            ItemMapper items = new ItemMapper(this);
            InventarioMapper invent = new InventarioMapper(this);
            this.game.setPosada(misiones.findMisions());
            this.game.setItemColection(items.findItems());
            this.game.getPersonaje().setInventario(invent.findItems());
            if (extra.getString("ITEM")!= null){
                Item it = this.game.getItemColection().get(extra.getString("ITEM"));
                this.game.getPersonaje().getInventario().put(it.getNombre(),it);
            }

        }
        else{
            this.game = extra.getParcelable("PARCELABLE");
            MisionMapper misiones = new MisionMapper(this);
            ItemMapper items = new ItemMapper(this);
            InventarioMapper invent = new InventarioMapper(this);
            this.game.setPosada(misiones.findMisions());
            this.game.setItemColection(items.findItems());
            this.game.getPersonaje().setInventario(invent.findItems());
        }





        View inventario = findViewById(R.id.Inventariobtn);
        inventario.setOnClickListener(this);

        View mision = findViewById(R.id.Misionbtn);
        mision.setOnClickListener(this);

        TextView vida = (TextView) findViewById(R.id.Vida);
        TextView fuerza = (TextView) findViewById(R.id.Fuerza);
        TextView resistencia = (TextView) findViewById(R.id.Resistencia);


        charname.setText(game.getPersonaje().getNombre());
        vida.setText("Vida: "+game.getPersonaje().getVida());
        fuerza.setText("Fuerza: "+game.getPersonaje().getFuerza());
        resistencia.setText("Resistencia: " + game.getPersonaje().getDefensa());

        Button camerabtn = (Button) findViewById(R.id.Fotobtn);
        camerabtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creamos el Intent para llamar a la Camara
                Intent cameraIntent = new Intent(
                        android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                //Creamos una carpeta en la memoria del terminal
                if (!Environment.getExternalStorageDirectory().getName().equalsIgnoreCase("CAdventure")) {
                    File imagesFolder = new File(Environment.getExternalStorageDirectory(), "CAdventure");
                    imagesFolder.mkdirs();
                }
                //añadimos el nombre de la imagen
                File image = new File(Environment.getExternalStorageDirectory()+"/Cadventure/", "foto.jpg");
                Uri uriSavedImage = Uri.fromFile(image);
                //Le decimos al Intent que queremos grabar la imagen
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage);
                //Lanzamos la aplicacion de la camara con retorno (forResult)
                startActivityForResult(cameraIntent, 1);
            }});


        

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //Comprobamos que la foto se ha realizado
        if (requestCode == 1 && resultCode == RESULT_OK) {
            //Creamos un bitmap con la imagen recientemente
            //almacenada en la memoria
            Bitmap bMap = BitmapFactory.decodeFile(
                    Environment.getExternalStorageDirectory() +
                            "/Cadventure/" + "foto.jpg");
            PersonajeMapper pers = new PersonajeMapper(this);
            this.game.getPersonaje().setAvatar(Environment.getExternalStorageDirectory() +
                    "/Cadventure/" + "foto.jpg");
            pers.modificar(this.game.getPersonaje());
            //Añadimos el bitmap al imageView para
            //mostrarlo por pantalla
            img.setImageBitmap(bMap);

        }
        if (requestCode == 2){
            if(resultCode ==RESULT_OK){
                ArrayList<String> modifs = getIntent().getStringArrayListExtra("result");
                for(String s: modifs){
                    Item it = this.game.getPersonaje().getInventario().get(s);
                    it.setEquipado();
                    //if (it.isEquipado()){

                   // }
                    this.game.getPersonaje().getInventario().put(s,it);
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        // your code.

        MisionMapper mis = new MisionMapper(this);
        InventarioMapper inv = new InventarioMapper(this);
        PersonajeMapper pers = new PersonajeMapper(this);
        StatMapper stat = new StatMapper(this);

        Collection<Mision>  misiones = this.game.getPosada().values();

        for(Mision m: misiones){
            mis.modificar(m);
        }
        Collection<Item> items = this.game.getPersonaje().getInventario().values();
        for(Item i: items){
            inv.modificar(i);
        }

        pers.modificar(this.game.getPersonaje());



    }






    @Override
    public void onClick(View v) {

        Intent i;
        switch (v.getId()){
            case R.id.Misionbtn:
                i = new Intent(this,MostrarMisionActivity.class);
                startActivity(i);
                break;

            case R.id.Inventariobtn:
               // i = new Intent(this,InventarioActivity.class);
                if(this.game.getPersonaje().getInventario().size()>0) {
                    i = new Intent(this, InventarioActivity.class);
                    startActivityForResult(i, 2);
                }
                else{
                    Toast t = new Toast(this);
                    t.makeText(this, "¡AUN NO TIENES ITEMS EN TU INVENTARIO!", Toast.LENGTH_LONG).show();
                }
                //startActivity(i);
                break;

        }



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_play_game, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




}
