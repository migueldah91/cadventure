package cm.cadventure;

/**
 * Clase que contiene toda la informacion de una partida
 * Created by Miguel on 28/04/2015.
 */
import android.os.Parcel;
import android.os.Parcelable;

import java.util.HashMap;


public class Partida implements Parcelable
{
    //private int id;

    public void setPersonaje(Personaje personaje) {
        this.personaje = personaje;
    }

    public void setPosada(HashMap<String, Mision> posada) {
        this.posada = posada;
    }

    public void setItemColection(HashMap<String, Item> itemColection) {
        this.itemColection = itemColection;
    }

    // private String user;

    //private int puntuacion;
    private Personaje personaje;
    private HashMap<String, Mision> posada;
    private HashMap<String, Item> itemColection;

    public Partida(){}

    public Partida(Personaje personaje) {
        this.personaje = personaje;
        this.posada = new HashMap<>();
        this.itemColection = new HashMap<>();

    }

    public Personaje getPersonaje() {
        return personaje;
    }

    public HashMap<String, Mision> getPosada() {
        return posada;
    }

    public HashMap<String, Item> getItemColection() {
        return itemColection;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(personaje,flags);
        dest.writeMap(posada);
        dest.writeMap(itemColection);

    }

    public static final Parcelable.Creator<Partida> CREATOR = new Parcelable.Creator<Partida>() {

        @Override
        public Partida createFromParcel(Parcel source) {
            return new Partida(source);
        }

        @Override
        public Partida[] newArray(int size) {
            return new Partida[size];
        }

    };



    private Partida(Parcel source) {
        this.personaje = source.readParcelable(Personaje.class.getClassLoader());
        this.posada = source.readHashMap(HashMap.class.getClassLoader());
        this.itemColection = source.readHashMap(HashMap.class.getClassLoader());

        //    this.unParcelable = source.readParcelable(OtraClase.class
        //          .getClassLoader());
    }


}
