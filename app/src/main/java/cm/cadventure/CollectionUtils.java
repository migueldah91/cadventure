package cm.cadventure;

/**
 * Created by Miguel on 16/05/2015.
 */
import java.util.HashMap;
import java.util.Map;

import android.os.Bundle;
import android.os.Parcelable;

public class CollectionUtils {
    public static Bundle toBundle(Map<String, ? extends Parcelable> input) {
        Bundle output = new Bundle();
        for(String key : input.keySet()) {
            output.putParcelable(key, input.get(key));
        }
        return output;
    }

    public static <T extends Parcelable> Map<String, T> fromBundle(Bundle input, Class<T> c) {
        Map<String, T> output = new HashMap<String, T>();
        for(String key : input.keySet()) {
            output.put(key, c.cast(input.getParcelable(key)));
        }
        return output;
    }
}
