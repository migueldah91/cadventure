package cm.cadventure;

/**
 * Created by Miguel on 28/04/2015.
 */
import android.os.Parcelable;


import android.os.Parcel;
import android.os.Parcelable;


import java.io.InputStream;

/**
 * Created by Carlos on 27/04/2015.
 */
public class Personaje implements Parcelable {
    private int id;
    private String nombre;
    private String avatar;
    private int vida;
    private int fuerza;
    private int defensa;
    private int puntuacion;

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }

    private Inventario inventario;

    // private Posada posada;
    // private int id;

    public Personaje(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getVida() {
        return vida;
    }

    public int getFuerza() {
        return fuerza;
    }

    public int getDefensa() {
        return defensa;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public Personaje(int id, String nombre, String avatar, int vida, int fuerza, int defensa, int puntuacion, Inventario inventario) {
        this.id = id;
        this.nombre = nombre;
        this.avatar = avatar;
        this.vida = vida;
        this.fuerza = fuerza;
        this.defensa = defensa;
        this.puntuacion = puntuacion;
        this.inventario = inventario;
        ///this.posada = posada;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public void setFuerza(int fuerza) {
        this.fuerza = fuerza;
    }

    public void setDefensa(int defensa) {
        this.defensa = defensa;
    }

    public int getPuntuacion() {
        return puntuacion;
    }

    public void setPuntuacion(int puntuacion) {
        this.puntuacion = puntuacion;
    }

    /**
     * Describe the kinds of special objects contained in this Parcelable's
     * marshalled representation.
     *
     * @return a bitmask indicating the set of special object types marshalled
     * by the Parcelable.
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * Flatten this object in to a Parcel.
     *
     * @param dest  The Parcel in which the object should be written.
     * @param flags Additional flags about how the object should be written.
     *              May be 0 or {@link #PARCELABLE_WRITE_RETURN_VALUE}.
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(nombre);
       // dest.writeMap(inventario);
        dest.writeString(avatar);
        dest.writeInt(vida);
        dest.writeInt(fuerza);
        dest.writeInt(defensa);
        dest.writeInt(puntuacion);
        dest.writeMap(inventario);
    }
    private Personaje(Parcel source) {
        this.id = source.readInt();
        this.nombre = source.readString();
        this.avatar = source.readString();
        this.vida = source.readInt();
        this.fuerza = source.readInt();
        this.defensa = source.readInt();
        this.puntuacion = source.readInt();
        source.readHashMap(Inventario.class.getClassLoader());



        //    this.unParcelable = source.readParcelable(OtraClase.class
        //          .getClassLoader());
    }

    public static final Parcelable.Creator<Personaje> CREATOR = new Parcelable.Creator<Personaje>() {

        @Override
        public Personaje createFromParcel(Parcel source) {
            return new Personaje(source);
        }

        @Override
        public Personaje[] newArray(int size) {
            return new Personaje[size];
        }

    };
}
