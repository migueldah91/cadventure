package cm.cadventure;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;


public class MainActivity extends Activity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        View play = findViewById(R.id.jugarbtn);
        play.setOnClickListener(this);
        View stats = findViewById(R.id.statbtn);
        stats.setOnClickListener(this);
        View about = findViewById(R.id.Aboutbtn);
        about.setOnClickListener(this);
        View exit = findViewById(R.id.exitbtn);
        exit.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.jugarbtn:
                i = new Intent(this,ChooseGameActivity.class);
                startActivity(i);
                break;
            case R.id.statbtn:
                i = new Intent(this,StatsActivity.class);
                i.putExtra("Stats",'s');
                startActivity(i);
                break;
            case R.id.Aboutbtn:
                i = new Intent(this,StatsActivity.class);
                i.putExtra("Stats", 'a');
                startActivity(i);
                break;
            case R.id.exitbtn:
                System.exit(0);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
