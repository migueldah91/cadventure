package cm.cadventure;

/**
 * Created by Miguel on 28/04/2015.
 */
import java.util.HashMap;

/**
 * Clase que simula el inventario del personaje
 */
public class Inventario extends HashMap<String,Item> {
    private int id;
    private boolean equipado;
    // private String item;
    private int cantidad;

    public Inventario(int id) {
        this.id = id;
        //   this.item = item;
        //  this.cantidad = cantidad;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    // public String getItem() {
    //   return item;
    //}

    //public void setItem(String item) {
    //  this.item = item;
    //}

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }
}
