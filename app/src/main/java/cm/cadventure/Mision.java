package cm.cadventure;

import java.io.InputStream;

/**
 * Clase con los atributos de una mision
 * Created by Miguel on 28/04/2015.
 */
public class Mision
{
    private String nombre;
    private String descripcion;
    private String recompensa;
    private String desbloqueo;
    private String imagen;
    private int estado;

    public Mision(String nombre, String descripcion, String recompensa, String desbloqueo, String imagen, int estado)
    {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.recompensa = recompensa;
        this.desbloqueo = desbloqueo;
        this.imagen = imagen;
        this.estado = estado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getRecompensa() {
        return recompensa;
    }

    public void setRecompensa(String recompensa) {
        this.recompensa = recompensa;
    }

    public String getDesbloqueo() {
        return desbloqueo;
    }

    public void setDesbloqueo(String desbloqueo) {
        this.desbloqueo = desbloqueo;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }
}
