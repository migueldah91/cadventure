package cm.cadventure;

import android.content.Context;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

/**
 * Created by Miguel on 15/05/2015.
 */
public class WebAppInterface {

    Context mContext;


    public WebAppInterface(Context c){
        this.mContext = c;
    }


    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
}
