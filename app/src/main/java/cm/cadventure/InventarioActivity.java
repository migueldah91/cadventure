package cm.cadventure;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import bbdd.ProyectoSQLiteHelper;


public class InventarioActivity extends ListActivity {

    private Partida game;
    private ArrayList<String> modificado = new ArrayList<String>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // String[] columnasDB = new String[]{"NOMBRE", "CANTIDAD", "EQUIPADO"};

        ProyectoSQLiteHelper bbdd = new ProyectoSQLiteHelper(this,"PROYECTO.db", null, 3);


        SQLiteDatabase db = bbdd.getWritableDatabase();
        Cursor cursor;

        //Si hemos abierto correctamente la base de datos
        if(db != null)
        {

            cursor = db.rawQuery(" SELECT NOMBRE AS _id, EFECTO, EQUIPADO FROM INVENTARIO", null);

            String[] desdeEstasColumnas = {"_id", "EFECTO", "EQUIPADO"};
            int[] aEstasViews = {R.id.nombreItem, R.id.efectoItem, R.id.equipadoItem};
            SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.item, cursor, desdeEstasColumnas, aEstasViews, 0);

            ListView listado = getListView();
            listado.setAdapter(adapter);


            //Cerramos la base de datos
            db.close();
        }

    }



    @Override
    public void onListItemClick(ListView lista, View view, int posicion, long id) {
        TextView checkBox = (TextView)findViewById(R.id.equipadoItem);

        TextView nombreItem = (TextView)findViewById(R.id.nombreItem);
        TextView mision = (TextView)view.findViewById(R.id.tituloMision);

        if (mision.getText() == "No Equipado"){
            mision.setText("Equipado");
            this.modificado.add(nombreItem.getText().toString());
        }

        else{
            mision.setText("No equipado");
            this.modificado.add(nombreItem.getText().toString());
        }



        ProyectoSQLiteHelper bbdd = new ProyectoSQLiteHelper(this,"DBPROYECTO", null, 1);
        SQLiteDatabase db = bbdd.getWritableDatabase();


        if(db != null)
        {
            db.execSQL("UPDATE INVENTARIO SET EQUIPADO = " + mision.getText() + "WHERE NOMBRE = " + nombreItem.getText());

            //Cerramos la base de datos
            db.close();
        }





    }



    @Override
    public void onBackPressed() {
        // your code.
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result",this.modificado);
        setResult(RESULT_OK,returnIntent);
        finish();


    }



       // setContentView(R.layout.activity_inventario);





       /** Object[] it = this.game.getPersonaje().getInventario().values().toArray();
        Item[] elementosguays = null;
        for(int i = 0; i< it.length; i++){
            elementosguays[i] = (Item)it[i];
        }

        ArrayAdapter<Item> aa = new ArrayAdapter<Item>(this,
                R.layout.item, elementosguays);
        setListAdapter(aa);
**/







    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_inventario, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
