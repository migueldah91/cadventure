package cm.cadventure;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import bbdd.StatMapper;


public class StatsActivity extends ActionBarActivity {

    private WebView myWview;
    private Stats stats;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);
        this.myWview = (WebView)findViewById(R.id.webView);
        WebSettings settings = this.myWview.getSettings();;
        settings.setJavaScriptEnabled(true);
        // Bind a new interface between your JavaScript and Android code
        this.myWview.addJavascriptInterface(new WebAppInterface(this), "Android");



        if (getIntent().getExtras().getChar("Stats") != 'a'){
            //this.myWview.loadUrl("file:///android_asset/estadisticas.html");
            StatMapper stm = new StatMapper(this);
            this.stats = stm.findById(1);
            String jugadas = "0";
            String acabadas = "0";
            String puntuacion = "0";
            if (this.stats != null) {
                 jugadas = Integer.toString(this.stats.getJugadas());
                 acabadas = Integer.toString(this.stats.getAcabadas());
                 puntuacion = Integer.toString(this.stats.getTotal());
            }
            //final String END_LINE = System.getProperty("line.Separator");
            String contenido = "Partidas jugadas: "+ jugadas +
                    "Partidas acabadas: "+acabadas +
                    "Puntuación acumulada: "+ puntuacion;

            this.myWview.loadDataWithBaseURL(null,contenido,"text/html","UTF-8",null);
           // this.myWview.loadUrl("javascript:refresh('"+jugadas+""+acabadas+""+puntuacion+"')");
        }

        else
            this.myWview.loadUrl("file:///android_asset/info.html");
    }


    public void OnBackPressed(){
        if(this.myWview.canGoBack()){
            this.myWview.goBack();
        }
        else
            super.onBackPressed();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_stats, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
