package cm.cadventure;

/**
 * Clase con la información de un item
 * Created by Miguel on 28/04/2015.
 */
public class Item
{
   // private int id;
    private String nombre;
    private String descripcion;
    private String efecto;
    private String equipado;
    private int cantidad;

    public Item(String nombre, String descripcion, String efecto) {
        this.cantidad = 0;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.efecto = efecto;
        this.equipado = "no equipado";
    }

    public String isEquipado() {
        return equipado;
    }

    public void setEquipado() {

        if (this.equipado.equals("equipado"))
            this.equipado = "no equipado";
        else
            this.equipado = "equipado";
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Item(String nombre, String equipado, int cantidad)
    {
        this.nombre = nombre;
        this.equipado = equipado;
        this.cantidad = cantidad;
        this.efecto = "";
        this.descripcion = "";

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getEfecto() {
        return efecto;
    }

    public void setEfecto(String efecto) {
        this.efecto = efecto;
    }
}
