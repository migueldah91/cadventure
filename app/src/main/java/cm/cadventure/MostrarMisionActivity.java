package cm.cadventure;

import android.app.ListActivity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import bbdd.MisionMapper;
import bbdd.ProyectoSQLiteHelper;


public class MostrarMisionActivity extends ListActivity  {


    private Partida game;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

      //  setContentView(R.layout.activity_mostrar_mision);

        ProyectoSQLiteHelper bbdd = new ProyectoSQLiteHelper(this,"PROYECTO.db", null, 3);


        SQLiteDatabase db = bbdd.getWritableDatabase();
        Cursor cursor;

        //Si hemos abierto correctamente la base de datos
        if(db != null)
        {

            cursor = db.rawQuery(" SELECT NOMBRE AS _id, DESCRIPCION FROM MISION", null);




                String[] desdeEstasColumnas = {"_id", "DESCRIPCION"};
                int[] aEstasViews = {R.id.tituloMision,R.id.descrMision};
                SimpleCursorAdapter adapter = new SimpleCursorAdapter(this, R.layout.mision, cursor, desdeEstasColumnas, aEstasViews, 0);


                ListView listado = getListView();
                listado.setAdapter(adapter);

            //Cerramos la base de datos
            db.close();
        }

        //this.game = getIntent().getParcelableExtra("PARCELABLE");




    }




    @Override
    public void onListItemClick(ListView lista, View view, int posicion, long id) {

        super.onListItemClick(lista, view, posicion, id);
        // Hacer algo cuando un elemento de la lista es seleccionado
        //String selectedItem = (String) getListView().getItemAtPosition(posicion);
        //btn.setOnClickListener((View.OnClickListener) view);

        TextView mision = (TextView)view.findViewById(R.id.tituloMision);
        String nombre = mision.getText().toString();
        Intent i = new Intent(this, PlayActivity.class);
        i.putExtra("MISION",nombre);
        startActivity(i);



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_mostrar_mision, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }




}
