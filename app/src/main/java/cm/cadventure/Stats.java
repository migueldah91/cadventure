package cm.cadventure;

/**
 * Contiene las estadísticas de la aplicación
 * Created by Carlos on 06/05/2015.
 */
public class Stats
{
    private int id;
    private int total;
    private int jugadas;
    private int acabadas;

    public Stats(int id, int total, int jugadas, int acabadas) {
        this.id = id;
        this.total = total;
        this.jugadas = jugadas;
        this.acabadas = acabadas;
    }

    public int getId(){
        return id;
    }

    public int getTotal() {

        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getJugadas() {
        return jugadas;
    }

    public void setJugadas(int jugadas) {
        this.jugadas = jugadas;
    }

    public int getAcabadas() {
        return acabadas;
    }

    public void setAcabadas(int acabadas) {
        this.acabadas = acabadas;
    }
}
