package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.HashMap;

import cm.cadventure.Pregunta;


/**
 * Clase con las operaciones de la tabla PREGUNTA
 * Created by Carlos on 27/04/2015.
 */
public class PreguntaMapper extends Mapper{
    private final String TABLE_NAME = "PREGUNTA";
    private final String[] COLUMN_NAME = {"ID", "DESCRIPCION", "MISION", "OPCION1", "OPCION2", "OPCION3", "ID1", "ID2", "ID3"};
    private final String KEY_COLUMN_NAME = "ID";


    public PreguntaMapper(Context context) {
        super(context);
    }


    public Pregunta findById(int id) {
        Pregunta p       = null;
        Cursor c = null;

        try {

            String[] key = {Integer.toString(id)};
            c= db.query(TABLE_NAME,COLUMN_NAME,KEY_COLUMN_NAME+"=?",key,null,null,null);
            if(c.moveToFirst())
                p = buildObject(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return p;
    }

    public ArrayList<Pregunta> buscaPreguntas(String mision)
    {
        ArrayList<Pregunta> preguntas = new ArrayList<>();
        Cursor cursor;

        String[] strings = {mision};
        cursor = db.rawQuery(" SELECT * FROM PREGUNTA WHERE MISION = ? ", strings);
        Pregunta p;
        if(cursor.moveToFirst()){
         do{
             p = buildObject(cursor);
             preguntas.add(p);

         }while(cursor.moveToNext());

        }



        return preguntas;
    }

    /**
     * Inserta una nueva fila en la tabla
     * @param ask
     * @return
     */
    public void insert (Pregunta ask)
    {
        ContentValues values = serializeObject(ask);

        try {
            db.insert(TABLE_NAME, "ID,DESCRIPCION,MISION,OPCION1,OPCION2,OPCION3,ID1,ID2,ID3", values);// -1 si fallo
        }catch (SQLException e){}

        //return ret;
    }

    /**
     * Modifica la tabla
     * @param ask
     * @throws SQLException
     */
    public void modificar(Pregunta ask) throws SQLException {

        String clause = " where " + KEY_COLUMN_NAME + "=? ";
        String[] key = {Integer.toString(ask.getId())};
        try {
            db.update(TABLE_NAME, serializeObject(ask), clause, key);
        } catch (SQLException e) {

        }
    }

    private Pregunta buildObject(Cursor cursor) {
        Pregunta ask = null;

        int id = cursor.getInt(0);
        String des = cursor.getString(1);
        String mis = cursor.getString(2);
        String o1 = cursor.getString(3);
        String o2 = cursor.getString(4);
        String o3 = cursor.getString(5);
        int id1 = cursor.getInt(6);
        int id2 = cursor.getInt(7);
        int id3 = cursor.getInt(8);

        ask = new Pregunta(id,des,o1,o2,o3,mis, id1, id2,id3);

        return ask;
    }

    private ContentValues serializeObject(Pregunta p) {
        ContentValues c  = new ContentValues();
            c.put("id",p.getId());
           c.put("descripcion",p.getDescripcion());
           c.put("opcion1", p.getOpc1());
           c.put("opcion2", p.getOpc2());
           c.put("opcion3", p.getOpc3());
           c.put("mision", p.getMision());
           c.put("id1", p.getId1());
           c.put("id2", p.getId2());
           c.put("id3", p.getId3());

        return c;
    }
}
