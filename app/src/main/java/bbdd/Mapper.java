package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.HashMap;

import javax.sql.DataSource;


/**
 * Created by Carlos on 27/04/2015.
 */
public abstract class Mapper
{
    protected SQLiteDatabase db;
    private ProyectoSQLiteHelper dbHelper;
    private Context context;


    public Mapper(Context context)
    {
        this.context = context;
        dbHelper = ProyectoSQLiteHelper.getHelper(context);
        open();
    }


    /**
     *
     * @throws SQLException
     */
    public void open() throws SQLException {
        if (dbHelper ==null)
            dbHelper = ProyectoSQLiteHelper.getHelper(context);
        this.db = dbHelper.getWritableDatabase();
    }

    /**
     *
     */
    public void close() {
        db.close();
        dbHelper.close();
    }



  }
