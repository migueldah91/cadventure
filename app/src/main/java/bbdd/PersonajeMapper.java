package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.util.HashMap;

import cm.cadventure.Personaje;

/**
 * Clase con las operaciones de la tabla PERSONAJE
 * Created by Carlos on 27/04/2015.
 */
public class PersonajeMapper extends Mapper
{
    private final String TABLE_NAME = "PERSONAJE";
    private final String[] COLUMN_NAME = {"ID","NOMBRE", "AVATAR", "VIDA", "FUERZA", "DEFENSA", "PUNTUACION"};
    private final String KEY_COLUMN_NAME = "ID";


    public PersonajeMapper(Context context) {
        super(context);
    }

    public Personaje findById (int id) {
        Personaje per       = null;
        Cursor c = null;


        try {
            String[] key = {Integer.toString(id)};
            String condition = KEY_COLUMN_NAME+"=?";
            c= db.query(TABLE_NAME,COLUMN_NAME,condition,key,null,null,null);
            if(c.moveToFirst())
                per = buildObject(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return per;
    }

    /**
     * Inserta una nueva fila en la tabla de personaje
     * @param p
     * @return
     */
    public void insert (Personaje p)
    {
        ContentValues values = serializeObject(p);

        try {
            db.insert(TABLE_NAME, "ID,NOMBRE,AVATAR,VIDA,FUERZA,DEFENSA,PUNTUACION", values);// -1 si fallo
        }catch (SQLException e){}
    }

    /**
     * Modifica la tabla
     * @param p
     * @throws SQLException
     */
    public void modificar(Personaje p) throws SQLException
    {

        String clause = " where "+ KEY_COLUMN_NAME+"=? ";
        String[] key = {Integer.toString(p.getId())};
        try {
            db.update(TABLE_NAME, serializeObject(p), clause, key);
        }catch (SQLException e){

        }

    }

    private Personaje buildObject(Cursor cursor) {
        Personaje p = null;

        int id = cursor.getInt(0);
        String n = cursor.getString(1);
        String avatar=cursor.getString(2);
        int vida = cursor.getInt(3);
        int fuerza = cursor.getInt(4);
        int defensa = cursor.getInt(5);
        int puntuacion = cursor.getInt(6);

        p = new Personaje(id, n,avatar, vida, fuerza, defensa, puntuacion, null);

        return p;
    }


    private ContentValues serializeObject(Personaje p) {
        ContentValues ret = new ContentValues();
            ret.put("id",p.getId());
            ret.put("nombre", p.getNombre());
            ret.put("avatar",p.getAvatar());
            ret.put("vida",p.getVida());
            ret.put("fuerza",p.getFuerza());
            ret.put("defensa",p.getDefensa());
            ret.put("puntuacion",p.getPuntuacion());

        return ret;
    }

}
