package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.util.HashMap;

import cm.cadventure.Pregunta;
import cm.cadventure.Stats;

/**
 * Clase con las operaciones de la tabla STATS
 * Created by Carlos on 06/05/2015.
 */
public class StatMapper extends Mapper
{
    private final String TABLE_NAME = "STATS";
    private final String[] COLUMN_NAME = {"ID","TOTAL", "JUGADAS", "ACABADAS"};
    private final String KEY_COLUMN_NAME = "ID";
    private static final int DATABASE_VERSION = 7;

    public StatMapper(Context context) {
        super(context);
    }

    public Stats findById(int id) {
        Stats s       = null;
        Cursor c = null;

        try {
            String[] key = {Integer.toString(id)};
            String condition = KEY_COLUMN_NAME+"=?";
            c= db.rawQuery("select * from STATS where ID = 1",null);
            if(c.moveToFirst())
                s = buildObject(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return s;
    }

    /**
     * Inserta una nueva fila en la tabla
     * @param stat
     * @return
     */
    public void insert (Stats stat)
    {
        ContentValues values = serializeObject(stat);

        try {
            db.insert(TABLE_NAME, "ID,TOTAL,JUGADAS,ACABADAS", values);// -1 si fallo
        }catch (SQLException e){}
    }

    /**
     * Modifica la tabla
     * @param stat
     * @throws SQLException
     */
    public void modificar(Stats stat) throws SQLException {

        String clause = " where " + KEY_COLUMN_NAME + "=? ";
        String[] key = {Integer.toString(stat.getId())};
        try {
            db.update(TABLE_NAME, serializeObject(stat), clause, key);
        } catch (SQLException e) {

        }
    }

    private Stats buildObject(Cursor cursor) {
    Stats stat = null;

    int id = cursor.getInt(0);
    int total = cursor.getInt(1);
    int jugadas = cursor.getInt(2);
    int acabadas = cursor.getInt(3);

    stat = new Stats(id,total,jugadas,acabadas);

    return stat;
    }

    private ContentValues serializeObject(Stats s) {
        ContentValues con = new ContentValues();
        con.put("id",s.getId());
        con.put("total",s.getTotal());
        con.put("jugadas", s.getJugadas());
        con.put("acabadas", s.getAcabadas());
        return con;
    }
}
