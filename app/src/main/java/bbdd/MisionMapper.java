package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.util.HashMap;

import cm.cadventure.Mision;

/**
 * Clase con las operaciones de la tabla MISION
 * Created by Carlos on 27/04/2015.
 */
public class MisionMapper extends Mapper
{
    private final String TABLE_NAME = "MISION";
    private final String[] COLUMN_NAME = {"NOMBRE","DESCRIPCION", "RECOMPENSA", "IMAGEN", "IDDESBLOQUEO", "ESTADO"};
    private final String KEY_COLUMN_NAME = "NOMBRE";


    public MisionMapper(Context context) {
        super(context);
    }

    public Mision findById(String id) {
        Mision m       = null;
        Cursor c = null;

        try {
            String[] key = {id};
            c= db.query(TABLE_NAME, COLUMN_NAME, KEY_COLUMN_NAME+"=?",key , null, null, null);

            if(c.moveToFirst())
                m = buildObject(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }
        return m;
    }

    public HashMap findMisions(){
        HashMap ret = new HashMap<>();
        Mision m = null;
        Cursor c = null;

        try {
            String consulta = "SELECT * FROM MISION;";
            c = this.db.rawQuery(consulta,null);
            if(c.moveToFirst())
                do {
                    m = buildObject(c);
                    ret.put(m.getNombre(), m);
                }while(c.moveToNext());
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return ret;
    }

    /**
     * Inserta una nueva fila en la tabla
     * @param mision
     * @return
     */
    public void insert (Mision mision)
    {
        ContentValues values = serializeObject(mision);

        try {
           /* String[] nombres =
                    {mision.getNombre(), mision.getDescripcion(), mision.getRecompensa(),mision.getImagen(),
                            mision.getDesbloqueo(),Integer.toString(mision.getEstado())};*/
           // String inserta = "INSERT INTO MISION VALUES (?,?,?,?,?,?)";
            db.insert(TABLE_NAME, null,values);

        }catch (SQLException e){
            System.out.println("fallo de insercion");}

        //return ret;
    }

    /**
     * Modifica la tabla
     * @param mision
     * @throws SQLException
     */
    public void modificar(Mision mision) throws SQLException
    {

        String clause = " where "+ KEY_COLUMN_NAME+"=? ";
        String[] key = {mision.getNombre()};
        try {
            db.update(TABLE_NAME, serializeObject(mision), clause, key);
        }catch (SQLException e){

        }

    }


    private Mision buildObject(Cursor cursor) {
        Mision mis = null;

        String name = cursor.getString(0);
        String des = cursor.getString(1);
        String rec = cursor.getString(2);
        String imagen = cursor.getString(3);
        String idD = cursor.getString(4);
        int est = cursor.getInt(5);

        mis = new Mision(name, des,rec,idD,imagen,est);

        return mis;
    }


    private ContentValues serializeObject(Mision m) {
        ContentValues cont  = new ContentValues();
            cont.put("nombre" ,m.getNombre());
            cont.put("descripcion",m.getDescripcion());
            cont.put("recompensa",m.getRecompensa());
            cont.put("imagen",m.getImagen());
            cont.put("iddesbloqueo",m.getDesbloqueo());
            cont.put("estado",m.getEstado());


        return cont;
    }


}
