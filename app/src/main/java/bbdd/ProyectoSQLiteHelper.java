package bbdd;
/**
 * Created by Carlos on 26/04/2015.
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ProyectoSQLiteHelper  extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "PROYECTO.db";
    //private static final int DATABASE_VERSION = 1;

    //Tabla usuario
    public static final String TABLE_USUARIO = "USUARIO";
    public static final String COLUMN_USER			= "USER";
    public static final String COLUMN_PASS 			= "PASS";
    public static final String COLUMN_AVATAR		= "AVATAR";

    //Tabla stats
    public static final String TABLE_STAT = "STATS";
    public static final String COLUMN_ID		= "ID";
    public static final String COLUMN_TOTAL	        = "TOTAL";
    public static final String COLUMN_JUGADAS       = "JUGADAS";
    public static final String COLUMN_ACABADAS      = "TERMINADAS";

    //Tabla personaje
    public static final String TABLE_PERSONAJE = "PERSONAJE";
    public static final String COLUMN_NOMBRE	= "NOMBRE";
    public static final String COLUMN_FUERZA 	= "FUERZA";
    public static final String COLUMN_VIDA	    = "VIDA";
    public static final String COLUMN_DEFENSA 	= "DEFENSA";
    public static final String COLUMN_PUNTUACION = "PUNTUACION";

    //Tabla inventario
    public static final String TABLE_INVENTARIO = "INVENTARIO";
    public static final String COLUMN_ITEM 			= "ITEM";
    public static final String COLUMN_CANTIDAD		= "CANTIDAD";
    public static final String COLUMN_EQUIPADO      = "EQUIPADO";

    //Tabla item
    public static final String TABLE_ITEM = "ITEM";
    public static final String COLUMN_DESCRIPCION	= "DESCRIPCION";
    public static final String COLUMN_EFECTO		= "EFECTO";

    //Tabla mision
    public static final String TABLE_MISION = "MISION";
    public static final String COLUMN_RECOMPENSA	= "RECOMPENSA";
    public static final String COLUMN_IMAGEN		= "IMAGEN";
    public static final String COLUMN_IDDESBLOQUEO	= "IDDESBLOQUEO";
    public static final String COLUMN_ESTADO		= "ESTADO";

    //Tabla pregunta
    public static final String TABLE_PREGUNTA = "PREGUNTA";
    public static final String COLUMN_OPCION1 = "OPCION1";
    public static final String COLUMN_OPCION2 = "OPCION2";
    public static final String COLUMN_OPCION3 = "OPCION3";
    public static final String COLUMN_MISION   = "MISION";
    public static final String COLUMN_ID1 = "ID1";
    public static final String COLUMN_ID2 = "ID2";
    public static final String COLUMN_ID3 = "ID3";

    // Database creation sql statement
    private static final String create_personaje =
            //Crea la tabla de personaje
            "create table PERSONAJE (ID integer primary key, NOMBRE text, AVATAR text not null, VIDA integer not null, FUERZA integer not null, DEFENSA integer not null, PUNTUACION integer not null);";
/**
            " DROP TABLE IF EXISTS PERSONAJE; create table "

            +TABLE_PERSONAJE+" ("
            +COLUMN_ID + " integer primary key, "
            + COLUMN_NOMBRE + " text, "
            + COLUMN_AVATAR + " text not null, "
            + COLUMN_VIDA + " integer not null, "
            + COLUMN_FUERZA + " integer not null, "
            + COLUMN_DEFENSA + " integer not null, "
            + COLUMN_PUNTUACION + " integer not null"
            + ");";**/
    private static final String create_invetario =

            //Crea la tabla del inventario

            "create table INVENTARIO (NOMBRE text primary key, CANTIDAD integer not null, EQUIPADO string not null);";
            /**+TABLE_INVENTARIO + " ("
            + COLUMN_NOMBRE + " text primary key, "
            + COLUMN_CANTIDAD + " integer not null,"
            + COLUMN_EQUIPADO + " string not null); ";
**/
    private static final String create_item =
            //Crea la tabla de Items

            " create table ITEM (NOMBRE text primary key, DESCRIPCION text, EFECTO text not null);";/**
            +TABLE_ITEM + " (" + COLUMN_NOMBRE
            + " text primary key, "
            + COLUMN_DESCRIPCION + " text, "
            +  COLUMN_EFECTO + " text not null); ";
**/
    private static final String create_mision =
            //Crea la tabla mision
            "create table MISION (NOMBRE text primary key, DESCRIPCION text not null, RECOMPENSA text not null, IMAGEN text not null, IDDESBLOQUEO text, ESTADO integer not null);";
/**
            + TABLE_MISION + " (" + COLUMN_NOMBRE
            + " text primary key, "
            + COLUMN_DESCRIPCION + " text not null, "
            + COLUMN_RECOMPENSA + " text not null, "
            + COLUMN_IMAGEN + " text not null, "
            + COLUMN_IDDESBLOQUEO + " text, "
            + COLUMN_ESTADO + " integer not null); ";
**/
    private static final String create_pregunta =

            //Crea la tabla de preguntas
            "create table PREGUNTA (ID integer primary key autoincrement, DESCRIPCION text not null, MISION text not null, OPCION1 text not null, OPCION2 text not null, OPCION3 text not null, ID1 text not null, ID2 text not null, ID3 text not null);";
            /**+TABLE_PREGUNTA + "(" + COLUMN_ID
            + " integer primary key autoincrement, "
            + COLUMN_DESCRIPCION + " text not null, "
            + COLUMN_MISION + " text not null, "
            + COLUMN_OPCION1 + " text not null, "
            + COLUMN_OPCION2 + " text not null, "
            + COLUMN_OPCION3 + " text not null, "
            + COLUMN_ID1 + " int , "
            + COLUMN_ID2 + " int , "
            + COLUMN_ID3 + " int); ";
**/
    private static final String create_stat =

            //Crea la tabla de preguntas
            "create table STAT (ID integer primary key, TOTAL int not null, JUGADAS int not null, ACABADAS int not null); ";
    /**
            +TABLE_STAT + "(" + COLUMN_ID
            + " integer primary key, "
            + COLUMN_TOTAL + " int not null, "
            + COLUMN_JUGADAS + " int not null, "
            + COLUMN_ACABADAS + " int not null); "
            ;
**/
    private static ProyectoSQLiteHelper instance;

    public static synchronized ProyectoSQLiteHelper getHelper(Context context) {
        if (instance == null)
            instance = new ProyectoSQLiteHelper(context);
        return instance;
    }

    /**
     *
     * @param context
     */
    private ProyectoSQLiteHelper(Context context) {

        super(context, DATABASE_NAME, null, 3);
    }

    /**
     * @param context
     * @param name
     * @param factory
     * @param version
     */
    public ProyectoSQLiteHelper(Context context, String name, CursorFactory factory, int version) {
        super(context, DATABASE_NAME, null, version);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            // db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    /* (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onCreate(android.database.sqlite.SQLiteDatabase)
     */
    @Override
    public void onCreate(SQLiteDatabase database)
    {
        database.execSQL(create_personaje);
        database.execSQL(create_invetario);
        database.execSQL(create_item);
        database.execSQL(create_stat);
        database.execSQL(create_pregunta);
        database.execSQL(create_mision);
    }

    /* (non-Javadoc)
     * @see android.database.sqlite.SQLiteOpenHelper#onUpgrade(android.database.sqlite.SQLiteDatabase, int, int)
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(ProyectoSQLiteHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data.");
      //  db.execSQL("DROP TABLE IF EXISTS " + TABLE_USUARIO);
       // db.execSQL("DROP TABLE IF EXISTS " + TABLE_PARTIDA);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PERSONAJE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INVENTARIO);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ITEM);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MISION);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_PREGUNTA );
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STAT);
       // db.execSQL("DROP TABLE IF EXISTS " + TABLE_POSADA);
        onCreate(db);
       /* if (newVersion>oldVersion)
            copyDatabase();*/
    }

    /*public copyDatabase()
    {

    }*/


}
