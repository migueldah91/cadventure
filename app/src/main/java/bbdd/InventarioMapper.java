package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cm.cadventure.Inventario;
import cm.cadventure.Item;

/**
 * Clase con las operaciones de la tabla INVENTARIO
 * Created by Carlos on 06/05/2015.
 */
public class InventarioMapper extends Mapper
{
    private final String TABLE_NAME = "INVENTARIO";
    private final String[] COLUMN_NAME = {"NOMBRE", "CANTIDAD", "EQUIPADO"};
    private final String KEY_COLUMN_NAME = "NOMBRE";

    public InventarioMapper(Context context) {
       super(context);
    }

    public Item findById(String id) {
        Item item       = null;
        Cursor c = null;

        try {
            String[] key = {id}; String condition = KEY_COLUMN_NAME+"=?";
            c= db.query(TABLE_NAME,COLUMN_NAME,condition,key,null,null,null);
            if(c.moveToFirst())
                item = buildObject(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return item;
    }


    protected Item buildObject(Cursor cursor) {
        Item i = null;
        String nombre = cursor.getString(0);
        int cantidad = cursor.getInt(1);
        String equipado = cursor.getString(2);

        i = new Item (nombre, equipado,cantidad);
        return i;
    }

    public Inventario findItems(){
        Inventario ret = new Inventario(1);
        Item i = null;

        try {
            Cursor c = this.db.query(TABLE_NAME, COLUMN_NAME, null, null, null, null, null);
            if(c.moveToFirst()){
                do{
                    i = buildObject(c);
                    ret.put(i.getNombre(), i);

                }while (c.moveToNext());
            }

        }catch(SQLException e){}

        return ret;
    }

    /**
     * Inserta una nueva fila en la tabla
     * @param i
     * @return
     */
    public void insert (Item i)
    {
        ContentValues values = serializeObject(i);

        try {
            db.insert(TABLE_NAME, "NOMBRE, CANTIDAD, EQUIPADO", values);// -1 si fallo
        }catch (SQLException e){}

        //return ret;
    }

    /**
     * Modifica la tabla
     * @param item
     * @throws SQLException
     */
    public void modificar(Item item) throws SQLException
    {

        String clause = " where "+ KEY_COLUMN_NAME+"=? ";
        String[] key = {item.getNombre()};
        try {
            db.update(TABLE_NAME, serializeObject(item), clause, key);
        }catch (SQLException e){

        }

    }


    private ContentValues serializeObject(Item i) {
        ContentValues ret = new ContentValues();
        ret.put("nombre", i.getNombre());
        ret.put("cantidad", i.getCantidad());
        ret.put("equipado", i.isEquipado());

        return ret;
    }


}
