package bbdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;

import java.sql.PreparedStatement;
import java.util.HashMap;

import cm.cadventure.Item;

/**
 * Clase con las operaciones de la tabla ITEM
 * Created by Carlos on 27/04/2015.
 */
public class ItemMapper extends Mapper {
    private final String TABLE_NAME = "ITEM";
    private final String[] COLUMN_NAME = {"NOMBRE", "DESCRIPCION", "EFECTO"};
    private final String KEY_COLUMN_NAME = "NOMBRE";


    public ItemMapper(Context context) {

        super(context);
    }

    public Item findById(String id) {
        Item item  = null;
        Cursor c = null;

        try {
            String[] key = {id};
            String condition = KEY_COLUMN_NAME+"=?";
            c= db.query(TABLE_NAME,COLUMN_NAME,condition,key,null,null,null);
            if(c.moveToFirst())
                item = buildObject(c);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return item;
    }

    public HashMap findItems(){
        HashMap ret = new HashMap<>();
        Item i = null;
        Cursor c = null;

        try {
            String consulta = "SELECT * FROM ITEM";
            c = this.db.rawQuery(consulta,null);

            if(c.moveToFirst()) {
                do {
                    i = buildObject(c);
                    ret.put(i.getNombre(), i);
                    //c.moveToNext();
                } while (c.moveToNext());
            }


            if(c.moveToFirst())
                 do{
                    i = buildObject(c);
                    ret.put(i.getNombre(), i);

                }while(c.moveToNext());

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                c.close();
            } catch (Exception e) {}
        }

        return ret;
    }

    /**
     * Inserta una nueva fila en la tabla
     * @param i
     * @return
     */
    public void insert (Item i)
    {
        ContentValues values = serializeObject(i);

        try {
            db.insert(TABLE_NAME, "NOMBRE, DESCRIPCION, EFECTO", values);
        }catch (SQLException e){}

        //return ret;
    }

    /**
     * Modifica la tabla
     * @param i
     * @throws SQLException
     */
    public void modificar(Item i) throws SQLException
    {

        String clause = " where "+ KEY_COLUMN_NAME+"=? ";
        String[] key = {i.getNombre()};
        try {
            db.update(TABLE_NAME, serializeObject(i), clause, key);
        }catch (SQLException e){

        }

    }

    private Item buildObject(Cursor cursor) {
        Item item;
        String n = cursor.getString(0);
        String des = cursor.getString(1);
        String ef = cursor.getString(2);
        item = new Item(n, des, ef);

        return item;
    }


    private ContentValues serializeObject(Item i) {
        ContentValues ret  = new ContentValues();
           ret.put("NOMBRE",i.getNombre());
           ret.put("DESCRIPCION",i.getDescripcion());
           ret.put("EFECTO",i.getEfecto());

        return ret;
    }

}
