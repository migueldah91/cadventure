package bbdd;

import cm.cadventure.Item;
import cm.cadventure.Mision;
import cm.cadventure.Pregunta;

/**
 * Clase que genera y guarda en la BBDD los Items y las misones
 * Created by Carlos on 12/05/2015.
 */
public class Genera
{
    private Mision m1;
   // private AbstractMapper <Mision,String> mm;
    private Pregunta p1;
    private Pregunta p2;
    private Pregunta p3;
    //private AbstractMapper <Pregunta, Integer> pm;
    private Item i1;
   // private AbstractMapper<Item,String> im;

    public Genera()
    {
        String des = "Es una mañana como otra cualquiera en el bosque";

        m1 = new Mision("Amanecer", des, "No hay recompensa", null,"mision1",0);

        p1 = new Pregunta(1,"Nuestro heroe esta dormido", "Despertar", "Seguir durmiendo","Llamar a su lacayo","Amanecer",2,1,3);
        p2 = new Pregunta(2,"Una vez levantado. Que hacer?", "Desayunar", "Salir al bosque", "Bostezar y volver a la cama", "Amanecer",3,0,1);
        p3 = new Pregunta(3,"Que desayunar?", "Pollo de la noche anterior", "Pan mohoso", "Salir al bosque", "Amanecer",0,0,0);

        i1 = new Item("Espada afilada", "Una espada capaz de cortar armaduras", "+5 de fuerza");

        }


public Mision getM1() {
        return m1;
        }

public Pregunta getP1() {
        return p1;
        }

public Pregunta getP2() {
        return p2;
        }

public Pregunta getP3() {
        return p3;
        }

public Item getI1() {
        return i1;
        }
        }
